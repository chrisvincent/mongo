MONGO_CONTAINER='MongoDB'
MONGO_IMAGE='mongo:3'
MONGO_DIR='/data/mongodb'

ssh -o StrictHostKeyChecking=no -T root@$2 << ENDSSH
    mkdir -p $MONGO_DIR
    docker stop $MONGO_CONTAINER
    docker rm $MONGO_CONTAINER
    docker pull $MONGO_IMAGE
    docker run -d \
        --name $MONGO_CONTAINER \
        --restart=unless-stopped \
        -p 27017:$4 \
        -e "LETSENCRYPT_EMAIL=$1" \
        -e "VIRTUAL_HOST=$3" \
        -e "VIRTUAL_PORT=27017" \
        -e "LETSENCRYPT_HOST=$3" \
        -e "MONGO_INITDB_ROOT_USERNAME=$5" \
        -e "MONGO_INITDB_ROOT_PASSWORD=$6" \
        -v $MONGO_DIR:/data/db \
        $MONGO_IMAGE
ENDSSH
