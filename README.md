# MongoDB Deployment Project

Base project for deploying dockerize MongoDB using [mongo:4](https://hub.docker.com/_/mongo/).

Built specifically to take maximum advantage of GitLab CI/CD.

# Requirements

* Fork this project and make any necessary modifications to `scripts/deploy-mongo.sh` and `.gitlab-ci.yml`.
* Add environment variables to GitLab's `Settings -> CI/CD -> Variables` for this project.
